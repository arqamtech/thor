var controller = require('./baseController');
var {
    cities
} = require('../model');

async function retrive(filter, sort) {
    let doc = await controller.retrive(filter, sort, "", cities);
    return doc;
}
async function create(data) {
    let doc = await controller.create(data, cities);
    return doc;
}

module.exports = {
    retrive: retrive,
    create: create,
};