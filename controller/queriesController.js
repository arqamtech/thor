var controller = require('./baseController');
var {
    queries
} = require('../model');

async function retrive(filter, sort) {
    let doc = await controller.retrive(filter, sort, "", queries);
    return doc;
}
async function create(data) {
    let doc = await controller.create(data, queries);
    return doc;
}

module.exports = {
    retrive: retrive,
    create: create,
};