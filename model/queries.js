var db = require('../dataContext').dbInstance;
var schema = new db.Schema({
    contact: {
        'gender': String,
        'city': String,
        'age': String,
    },
    'stage': String,
    'score': String,
    'userId': String,
});

module.exports = db.model('queries', schema);